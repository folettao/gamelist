# Game List
List games in web application with Java language in back-end.

### Sumary
* 1\. [Technologies](#Technologies)
* 2\. [Domain model SQL](#Domain-model-SQL)
* 3\. [Changue between database](#Changue-between-database)
* 4\. [Docker](#Docker)

## Technologies
- Java 17
- Spring 3.1.1
  - Boot
  - JPA
- Maven 4
- Docker
- PostgreSQL

## Domain model SQL
![Domain model SQL](src/main/resources/gameList-model.png)

## Changue between database
In line 01 on the file _application.properties_ change _CHANGEWHERE_ for [test | dev | prod]:

```properties
spring.profiles.active=${APP_PROFILE:CHANGEWHERE}
```

## Docker
Run the command inside the folder where the _.yml_ file is.

```bash
docker-compose up -d 
```

# Run
Just run the project in the Intellij IDE and interact with Postman (the configs files are in the postman folder).
